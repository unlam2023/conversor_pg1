/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package conversor;

/**
 *
 * @author g.guzman
 */
public class ConversorGramoKilo extends ConversorGenerico {
    private static final double UN_KILO = 1000;
    
    @Override
    public String getLabel1() {
        return "Gramo";
    }
    
    @Override
    public String getLabel2() {
        
        return "Kilo";
    }
    

    @Override
    public Double convertirValor1Valor2(Double gramo) {
        return  gramo / UN_KILO;
    }

    @Override
    public Double convertirValor2Valor1(Double kilo) {
        return  kilo * UN_KILO;
    }

}
