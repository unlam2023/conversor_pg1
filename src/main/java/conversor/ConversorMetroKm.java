/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package conversor;

/**
 *
 * @author g.guzman
 */
public class ConversorMetroKm extends ConversorGenerico {
    
    private static final double UN_KILOMETRO = 1000;
    
    @Override
    public String getLabel1() {
        return "Metro";
    }
    
    @Override
    public String getLabel2() {
        
        return "KM";
    }
    

    @Override
    public Double convertirValor1Valor2(Double metros) {
        return  metros / UN_KILOMETRO;
    }

    @Override
    public Double convertirValor2Valor1(Double kms) {
        return  kms * UN_KILOMETRO;
    }

}
