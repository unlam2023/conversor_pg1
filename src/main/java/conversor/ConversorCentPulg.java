
package conversor;

public class ConversorCentPulg extends ConversorGenerico {

    private static final double UNA_PULGADA = 2.54;
    
    @Override
    public String getLabel1() {
        return "Centímetros"; 
    }

    @Override
    public String getLabel2() {
        
        return "Pulgadas";
    }

    @Override
    public Double convertirValor1Valor2(Double cent) {
        return cent / UNA_PULGADA;
    }

    @Override
    public Double convertirValor2Valor1(Double pulg) {
        return pulg * UNA_PULGADA;
    }
    
}
