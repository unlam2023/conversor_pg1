/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package conversor;

/**
 *
 * @author g.guzman
 */
public class ConversorGigabyteTerabyte extends ConversorGenerico {
    
    private static final double UN_TERABYTE = 1024;
    
    @Override
    public String getLabel1() {
        return "Gigabyte";
    }
    
    @Override
    public String getLabel2() {
        
        return "Terabyte";
    }
    
    @Override
    public String getTipo() {
        return this.getLabel1()+"/"+getLabel2();
    }

    @Override
    public Double convertirValor1Valor2(Double giga) {
        return  giga / UN_TERABYTE;
    }

    @Override
    public Double convertirValor2Valor1(Double tera) {
        return  tera * UN_TERABYTE;
    }

}
