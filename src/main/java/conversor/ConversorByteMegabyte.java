/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package conversor;

/**
 *
 * @author g.guzman
 */
public class ConversorByteMegabyte extends ConversorGenerico {
    
    private static final double UN_MEGABYTE = 1048576;
    
    @Override
    public String getLabel1() {
        return "Byte";
    }
    
    @Override
    public String getLabel2() {
        
        return "MegaByte";
    }
    
    @Override
    public String getTipo() {
        return this.getLabel1()+"/"+getLabel2();
    }

    @Override
    public Double convertirValor1Valor2(Double bytes) {
        return  bytes / UN_MEGABYTE;
    }

    @Override
    public Double convertirValor2Valor1(Double megabyte) {
        return  megabyte * UN_MEGABYTE;
    }

}
