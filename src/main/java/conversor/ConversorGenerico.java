
package conversor;

public abstract class ConversorGenerico {

    public abstract String getLabel1();
    public abstract String getLabel2();
    
    public abstract Double convertirValor1Valor2(Double valor1);
    public abstract Double convertirValor2Valor1(Double valor2);
    
     
    public String getTipo() {
        return getLabel1()+"/"+getLabel2();
    }
}
