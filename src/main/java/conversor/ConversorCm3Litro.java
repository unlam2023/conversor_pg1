/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package conversor;

/**
 *
 * @author g.guzman
 */
public class ConversorCm3Litro extends ConversorGenerico {
    private static final double UN_LITRO = 1000;

    @Override
    public String getLabel1() {
        return "Cm3";
    }
    
    @Override
    public String getLabel2() {
        
        return "Litro";
    }
    

    @Override
    public Double convertirValor1Valor2(Double cm3) {
        return  cm3 / UN_LITRO;
    }

    @Override
    public Double convertirValor2Valor1(Double lt) {
        return  lt * UN_LITRO;
    }

}
