
package conversor;

import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.List;
import javax.swing.JOptionPane;


public class ConversorGUI extends javax.swing.JFrame {
    
 private ConversorGenerico convesorSelected;
 private boolean convertirAValor2;

List<ConversorGenerico> listaConversores;
List<String> NUM_INVALIDOS = Arrays.asList("F","f","D","d");
  
    public ConversorGUI() {
        initComponents();
        
        this.setTitle("El Conversor");
        this.setLocationRelativeTo(null);
        
        setListaConversores();
        
        //Cargo los datos del combo
        for (ConversorGenerico conversor : listaConversores) {
            conversoresComboBox.addItem(conversor.getTipo());
        }
        
        setLabels();
        setInputs();
      
    }
    
    
    private void setListaConversores(){
        listaConversores = Arrays.asList(
                new ConversorByteMegabyte(),            
                new ConversorCentPulg(),
                new ConversorCm3Litro(),
                new ConversorGigabyteTerabyte(),        
                new ConversorGramoKilo(),
                new ConversorMetroKm(),
                new ConversorSegHora()                            
                );                    
    }

    
    private void setLabels(){
     int selectedIndex = conversoresComboBox.getSelectedIndex();
        convesorSelected = listaConversores.get(selectedIndex);
        this.jLabel1.setText(convesorSelected.getLabel1().concat(":"));
        this.jLabel2.setText(convesorSelected.getLabel2().concat(":"));
    }
    
    private void setInputs(){
        this.valor1TextField.setText(null);
        this.valor2TextField.setText(null);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        valor1TextField = new javax.swing.JTextField();
        convertirButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        valor2TextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        conversoresComboBox = new javax.swing.JComboBox<>();
        comboBoxLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 0, 0));

        valor1TextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                valor1TextFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                valor1TextFieldFocusLost(evt);
            }
        });
        valor1TextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                valor1TextFieldKeyPressed(evt);
            }
        });

        convertirButton.setText("Convertir");
        convertirButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                convertirButtonActionPerformed(evt);
            }
        });
        convertirButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                convertirButtonKeyPressed(evt);
            }
        });

        jLabel1.setText("Label 1");

        valor2TextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                valor2TextFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                valor2TextFieldFocusLost(evt);
            }
        });
        valor2TextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                valor2TextFieldKeyPressed(evt);
            }
        });

        jLabel2.setText("Label 2");

        conversoresComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                conversoresComboBoxActionPerformed(evt);
            }
        });

        comboBoxLabel.setText("Tipo de Conversión");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboBoxLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(valor2TextField, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(valor1TextField, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(43, 43, 43)
                                .addComponent(conversoresComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(114, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(convertirButton, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(conversoresComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBoxLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valor1TextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(3, 3, 3)
                .addComponent(convertirButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valor2TextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(44, 44, 44))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
       
    private void convertirButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_convertirButtonActionPerformed
        convertirButton();
    }//GEN-LAST:event_convertirButtonActionPerformed

    private void convertirButton() {
        if (convertirAValor2) {
            convertir(valor1TextField,valor2TextField );
        } else {
            convertir(valor2TextField,valor1TextField );
        }
    }
    
    private void convertir(javax.swing.JTextField origen, javax.swing.JTextField destino) throws HeadlessException {
        
        String valor1 = origen.getText().replace(',', '.');
               
        Double valor1Double;
        
        try {
            
            if( valor1.isEmpty() ){
                JOptionPane.showMessageDialog(this, "el campo no puede estar vacío", "Error al convertir", JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            if (NUM_INVALIDOS.contains(valor1.substring(valor1.length() - 1))){
                JOptionPane.showMessageDialog(this, "el campo contiene una f o una d", "Error al convertir", JOptionPane.ERROR_MESSAGE);
                return;
            }

            valor1Double = Double.valueOf(valor1);
           
        } catch (NumberFormatException ex) {
            System.out.println("Error al convertir ==> " + ex.getMessage());
            JOptionPane.showMessageDialog(this, "Error al convertir, caracteres invalidos", "Error al convertir",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        Double valor2;
        
        if(convertirAValor2){
            valor2 = convesorSelected.convertirValor1Valor2(valor1Double);
        }else{
            valor2 = convesorSelected.convertirValor2Valor1(valor1Double);
        }    
        
        destino.setText(String.format("%.2f", valor2));
    }

    private void valor1TextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_valor1TextFieldFocusLost
        convertirAValor2 = true;
    }//GEN-LAST:event_valor1TextFieldFocusLost

    private void valor2TextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_valor2TextFieldFocusLost
        convertirAValor2 = false;
    }//GEN-LAST:event_valor2TextFieldFocusLost

    private void valor1TextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_valor1TextFieldKeyPressed
        
        if (evt.getKeyChar()==KeyEvent.VK_ENTER) {
            convertir(valor1TextField,valor2TextField );
        }
    }//GEN-LAST:event_valor1TextFieldKeyPressed

    private void conversoresComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_conversoresComboBoxActionPerformed
        setLabels();
        setInputs();
    }//GEN-LAST:event_conversoresComboBoxActionPerformed

    private void valor2TextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_valor2TextFieldKeyPressed
       if (evt.getKeyChar()==KeyEvent.VK_ENTER) {
            convertir(valor2TextField,valor1TextField );
        }
    }//GEN-LAST:event_valor2TextFieldKeyPressed

    private void valor1TextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_valor1TextFieldFocusGained
        convertirAValor2 = true;
    }//GEN-LAST:event_valor1TextFieldFocusGained

    private void valor2TextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_valor2TextFieldFocusGained
        convertirAValor2 = false;
    }//GEN-LAST:event_valor2TextFieldFocusGained

    private void convertirButtonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_convertirButtonKeyPressed
          if (evt.getKeyChar()==KeyEvent.VK_ENTER) {
            convertirButton();
        }
    }//GEN-LAST:event_convertirButtonKeyPressed

    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ConversorGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel comboBoxLabel;
    private javax.swing.JComboBox<String> conversoresComboBox;
    private javax.swing.JButton convertirButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField valor1TextField;
    private javax.swing.JTextField valor2TextField;
    // End of variables declaration//GEN-END:variables
}
