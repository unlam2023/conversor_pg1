/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package conversor;

/**
 *
 * @author g.guzman
 */
public class ConversorSegHora extends ConversorGenerico {
    private static final double UNA_HORA = 3600;

    @Override
    public String getLabel1() {
        return "Segundos";
    }
    
    @Override
    public String getLabel2() {
        
        return "Horas";
    }
    
    @Override
    public Double convertirValor1Valor2(Double hs) {
        return  hs / UNA_HORA;
    }

    @Override
    public Double convertirValor2Valor1(Double seg) {
        return  seg * UNA_HORA;
    }

}
